import os
import sys


sys.path.insert(0, os.path.dirname(__file__))

# wsgi = imp.load_source('wsgi', 'app.py')
import importlib.machinery, importlib.util
loader = importlib.machinery.SourceFileLoader('wsgi', 'app.py')
spec = importlib.util.spec_from_loader(loader.name, loader)
my_module = importlib.util.module_from_spec(spec)
loader.exec_module(my_module)
application = my_module.app
